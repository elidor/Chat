package Chat.Verschlüsselung;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephan von Greiffenstern on 09.12.14.
 */
public class RSA {
    private final BigInteger p;
    private final BigInteger q;

    private final BigInteger n;
    private final BigInteger phi_n;
    private final BigInteger e;
    private BigInteger d;


    /**
     * n, e
     * gibt den öffentlichen Schlüssel zurück
     * @return
     */
    public BigInteger[] getPublicKey() {
        BigInteger[] bi = new BigInteger[2];
        bi[0] = n;
        bi[1] = e;
        return bi;
    }

    /**
     * synchronized
     *
     * @param pMessage
     * @param pBI      null wenn eigener public Key
     * @return
     */
    public synchronized BigInteger encrypt(BigInteger pMessage, BigInteger[] pBI) {
        BigInteger ee, nn;
        if (pBI != null) {
            ee = pBI[1];
            nn = pBI[0];
        } else { //falls mit dem eigenen Public Key verschlüsselt werden soll
            ee = e;
            nn = n;

        }
        return pMessage.modPow(ee, nn);
    }

    /**
     * synchronized
     */
    public synchronized BigInteger decrypt(BigInteger pMessage) {
        return pMessage.modPow(d, n);
    }

    private BigInteger generatePrime(int pStellen) {
        //pStellen gibt an wie lang die Zahl sein soll
        BigInteger i;
        while (true) {
            i = BigInteger.probablePrime(pStellen, new SecureRandom());
            if (i.isProbablePrime(40))
                break;
        }
        return i;
    }

    private BigInteger generateE(BigInteger pPhiN) {
        BigInteger i;
        int stellen = pPhiN.toString().length();

        while (true) {
            i = BigInteger.probablePrime(stellen - 1, new SecureRandom());
            if (i.gcd(pPhiN).compareTo(BigInteger.ONE) == 0) {
                if (i.compareTo(p) != 0 && i.compareTo(q) != 0) {
                    if (i.subtract(BigInteger.ONE).compareTo(BigInteger.ZERO) != 0) {
                        break;
                    }
                }
            }
        }
        return i;
    }

    /**
     * @param a e
     * @param b phi(n)
     * @return d
     */
    public BigInteger erEucl(BigInteger a, BigInteger b) {
        //a, b, q, r, x, y
        List<BigInteger[]> al = new ArrayList<BigInteger[]>();
        boolean bool = true;

        BigInteger[] lt = new BigInteger[6];
        lt[0] = a;
        lt[1] = b;
        lt[3] = a.mod(b); //rest a%b
        lt[2] = (a.subtract(lt[3]).divide(b));
        al.add(lt);

        //erster Durchlauf
        while (bool) {
            BigInteger[] alt = al.get(al.size() - 1);

            if (alt[3] == BigInteger.ZERO) {
                bool = false;
            } else {
                BigInteger[] l = new BigInteger[6];
                l[0] = alt[1]; //a
                l[1] = alt[3]; //b
                l[3] = l[0].mod(l[1]); //r
                l[2] = (l[0].subtract(l[3])).divide(l[1]); //q
                al.add(l);
            }
        }

        //x&y berechnen.
        al.get(al.size() - 1)[4] = BigInteger.ZERO;
        al.get(al.size() - 1)[5] = BigInteger.ONE;
        for (int i = al.size() - 2; i >= 0; i--) {
            al.get(i)[4] = al.get(i + 1)[5]; //x(i) = y(i+1)
            al.get(i)[5] = al.get(i + 1)[4].subtract((al.get(i)[2].multiply(al.get(i + 1)[5]))); // y(i) = x(i+1)-q(i)*y(i+1)
        }
        String s = al.get(al.size() - 2)[3].toString() + " = " + al.get(0)[4].toString() + "*" + al.get(0)[0].toString() +
                " + (" + al.get(0)[5].toString() + ")*" + al.get(0)[1].toString(); //x*a+y*b=r
        BigInteger bi = al.get(0)[4];
        if (bi.compareTo(BigInteger.ZERO) > 0) {
            return bi;
        } else {
            return bi.add(phi_n);
        }
    }


    public BigInteger erEucl2(BigInteger a, BigInteger b) {
        //a, b, q, r, x, y
        List<BigInteger[]> al = new ArrayList<BigInteger[]>();
        boolean bool = true;

        BigInteger[] lt = new BigInteger[6];
        lt[0] = a;
        lt[1] = b;
        lt[3] = a.mod(b); //rest a%b
        lt[2] = (a.subtract(lt[3]).divide(b));
        al.add(lt);

        //erster Durchlauf
        while (bool) {
            BigInteger[] alt = al.get(al.size() - 1);

            if (alt[3] == BigInteger.ZERO) {
                bool = false;
            } else {
                BigInteger[] l = new BigInteger[6];
                l[0] = alt[1]; //a
                l[1] = alt[3]; //b
                l[3] = l[0].mod(l[1]); //r
                l[2] = (l[0].subtract(l[3])).divide(l[1]); //q
                al.add(l);
            }
        }

        //x&y berechnen.
        al.get(al.size() - 1)[4] = BigInteger.ZERO;
        al.get(al.size() - 1)[5] = BigInteger.ONE;
        for (int i = al.size() - 2; i >= 0; i--) {
            al.get(i)[4] = al.get(i + 1)[5]; //x(i) = y(i+1)
            al.get(i)[5] = al.get(i + 1)[4].subtract((al.get(i)[2].multiply(al.get(i + 1)[5]))); // y(i) = x(i+1)-q(i)*y(i+1)
        }
        String s = al.get(al.size() - 2)[3].toString() + " = " + al.get(0)[4].toString() + "*" + al.get(0)[0].toString() +
                " + (" + al.get(0)[5].toString() + ")*" + al.get(0)[1].toString(); //x*a+y*b=r
        BigInteger bi = al.get(0)[4];

        for (BigInteger[] bbii : al) {
            for (BigInteger t2 : bbii) {
                System.out.print(" " + t2.toString());
            }
            System.out.println("");
        }

        if (bi.compareTo(BigInteger.ZERO) > 0) {
            return bi;
        } else {
            return bi.add(phi_n);
        }
    }

    public RSA(BigInteger pP, BigInteger pQ) {
        if (pP.isProbablePrime(40) && pQ.isProbablePrime(40)) {
            p = pP;
            q = pQ;
            System.out.println("Diese Primzahlen sind mit hoher Wahrscheinlichkeit nicht sicher!");
        } else {
            p = generatePrime(2048); //2048 bit sollten eigentlich sicher sein. 1024 sind auch sicher, aber 2048 ist eine schönere Zahl :)
            q = generatePrime(2048); //same here
        }
        n = p.multiply(q);
        phi_n = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        e = generateE(phi_n);
        d = e.modInverse(phi_n); //Vermutlich deutlich besserer Code, den ich allerdings nicht selbst geschrieben habe
        //d = erEucl(e, phi_n); //Mein Code
        /*System.out.println("p: " + p.toString());
        System.out.println("q: " + q.toString());
        System.out.println("n: " + n.toString());
        System.out.println("Phi(n): " + phi_n.toString());*/
        System.out.println("Schlüsselpaar erfolgreich berechnet!");
    }

    /**
     * &Uuml;berpr&uuml;fung f&uuml;r eigene Rechnung
     * @param pP beliebige Prinzahl
     * @param pQ beliebige Primzahl ≠ p
     * @param pE
     */
    public RSA(BigInteger pP, BigInteger pQ, BigInteger pE) {
        p = pP;
        q = pQ;
        e = pE;
        n = p.multiply(q);
        phi_n = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        d = erEucl2(e, phi_n);
    }
}
