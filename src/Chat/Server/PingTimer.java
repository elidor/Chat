package Chat.Server;

import java.util.ArrayList;
import java.util.TimerTask;

/**
 * Created by Stephan von Greiffenstern on 26.02.15.
 */
public class PingTimer extends java.util.TimerTask {

    Server s;
    @Override
    public void run() {
        s.sendAllClients("#ping:");
        s.sendList();
        ArrayList<ClientManager> al = s.getList();

        for (int i = 0; i < al.size(); i++) {
            al.get(i).checkIfActive();
        }
    }

    public PingTimer(Server pServer) {
        s=pServer;
    }
}
