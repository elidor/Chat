package Chat.Server;

import Chat.Verschlüsselung.RSA;

import java.io.*;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Timer;

/**
 * Created by Stephan von Greiffenstern on 24.02.15.
 */
public class Server {

    ServerSocket server;
    //ArrayList<PrintWriter> list_clientWriter;
    ArrayList<ClientManager> list_Clients;

    private RSA r;
    private Timer timer; //sendet Pings etc

    final int LEVEL_ERROR = 1; //System.errnew String(decr.toByteArray(), "UTF-8");
    final int LEVEL_NORMAL = 0; //System.out
    static int id = 0;

    public ArrayList<ClientManager> getList() {
        return list_Clients;
    }

    public static void main(String[] args) {

        Server s = new Server();
        if (s.runServer()) {
            s.listenToClients();
        }
    }

    public class ClientHandler implements Runnable {        //INNER CLASS

        Socket client;
        BufferedReader reader;
        int id = 0;
        boolean schluesselUebertragen = false;

        public void setID(int pID) {
            id = pID;
        }

        public ClientHandler(Socket client) {
            try {
                this.client = client;
                reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void setPubKey(BigInteger[] pBI) {
            for (int i = 0; i < list_Clients.size(); i++) {
                if (list_Clients.get(i).getId() == id) {
                    System.out.println("Client " + i + " hat Schlüssel übertragen");
                    //Client Public Key speichern
                    list_Clients.get(i).setPubKey(pBI);

                    //eigenen Public Key übertragen
                    BigInteger[] pkey = r.getPublicKey();
                    list_Clients.get(i).getPw().write("#pk:" + pkey[0] + "##" + pkey[1] + "\n");
                    list_Clients.get(i).getPw().flush();
                    System.out.println("eigenen Key an Client " + i + " übertragen");
                    schluesselUebertragen = true;
                }
            }
        }

        @Override
        public void run() {
            String nachricht;

            try {
                while ((nachricht = reader.readLine()) != null) {
                    if (nachricht.contains("#pk:")) {
                        String msg = nachricht.replaceAll("#pk:", "");
                        String[] msgPart = msg.split("##");
                        BigInteger[] bi = new BigInteger[2];
                        bi[0] = new BigInteger(msgPart[0]);
                        bi[1] = new BigInteger(msgPart[1]);
                        setPubKey(bi);
                    } else if (schluesselUebertragen) {
                        BigInteger bi = new BigInteger(nachricht);
                        BigInteger decr = r.decrypt(bi);                        //entschlüsseln
                        String msg = new String(decr.toByteArray(), "UTF-8");   //entschlüsselten Text zu String konvertieren
                        System.out.println("empfangen: " + msg);
                        processMsg(msg, id);
                        //sendAllClients(msg);
                    } else {
                        System.out.println("Unverschlüsselt empfangen!");
                        toConsole("Vom Client: \n" + nachricht, LEVEL_NORMAL);
                        sendAllClients(nachricht);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }                                                       //INNER CLASS OVER

    public void listenToClients() {
        while (true) {
            try {
                Socket client = server.accept();

                PrintWriter writer = new PrintWriter(client.getOutputStream());
                //list_clientWriter.add(writer);
                id++;
                list_Clients.add(new ClientManager(writer, id));
                System.out.println(list_Clients.get(list_Clients.size() - 1).getId() + " " + id);

                ClientHandler ch = new ClientHandler(client);
                ch.setID(id);
                Thread clientThread = new Thread(ch);

                clientThread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean runServer() {
        try {
            server = new ServerSocket(5555);
            toConsole("Server wurde gestartet!", LEVEL_ERROR);

            //list_clientWriter = new ArrayList<PrintWriter>();
            list_Clients = new ArrayList<ClientManager>();

            return true;
        } catch (IOException e) {
            toConsole("Server konnte nicht gestartet werden!", LEVEL_ERROR);
            e.printStackTrace();
            return false;
        }
    }

    public void toConsole(String message, int level) {
        if (level == LEVEL_ERROR) {
            System.err.println(message + "\n");
        } else {
            System.out.println(message + "\n");
        }
    }

    private int getIndexFromID(int pID) {
        for (int i = 0; i < list_Clients.size(); i++) {
            if (list_Clients.get(i).getId() == pID) {
                return i;
            }
        }

        return -1;
    }

    /**
     * sorgt dafür, dass alle inaktiven rausgeschmissen werden
     *
     * @param pInd
     */
    private boolean checkListAtIndex(int pInd) {
        boolean b = false;

        while (!b) {
            if (pInd < list_Clients.size()) {
                b = list_Clients.get(pInd).isActive();
                if (!b) {
                    System.out.println(list_Clients.get(pInd).getName() + " wegen Inaktivität gekickt.");
                    list_Clients.remove(pInd);
                }
            } else {
                b = true;

                if (list_Clients.isEmpty()) {
                    id = 1;
                    System.out.println("ID resettet");
                }

            }
        }
        if (pInd < list_Clients.size()) {
            return true;
        }
        return false;
    }

    private void processMsg(String msg, int id) {
        int ind = getIndexFromID(id);
        if (msg.startsWith("#nm:")) {
            String s = msg.replaceAll("#nm:", "");
            list_Clients.get(ind).setName(s);
            sendToOne("System: Name gesetzt", ind);

        } else if (msg.startsWith("#ping:")) {
            list_Clients.get(ind).processPing();
        } else if (msg.startsWith("#kick:")) {
            String s = msg.replaceAll("#kick:", "");
            if (list_Clients.get(ind).getName().equals("admin")) {
                for (int i = 0; i < list_Clients.size(); i++) {
                    if (list_Clients.get(i).getName().equals(s)) {
                        sendToOne("System: Du wurdest vom Server gekickt!", i);
                        list_Clients.remove(i);
                        sendAllClients("System: " + s + " wurde vom Server gekickt.");
                    }
                }
            }
        } else {
            sendAllClients(list_Clients.get(ind).getName() + ": " + msg);
        }
    }

    /**
     * Nachricht und Index in list_Clients übergeben
     *
     * @param msg
     * @param pIndex
     */
    private void sendToOne(String msg, int pIndex) {
        BigInteger klar = new BigInteger(msg.getBytes());
        BigInteger geheim = r.encrypt(klar, list_Clients.get(pIndex).getPubKey());
        list_Clients.get(pIndex).getPw().println(geheim.toString());
        list_Clients.get(pIndex).getPw().flush();
    }

    /**
     * Verschickt die aktuelle Nutzerliste
     */
    public void sendList() {
        if (list_Clients.size() > 0) {
            StringBuffer sb = new StringBuffer();
            sb.append("#list:");
            sb.append(list_Clients.get(0).getName());
            for (int i = 1; i < list_Clients.size(); i++) {
                sb.append(",");
                sb.append(list_Clients.get(i).getName());
            }
            sendAllClients(sb.toString());
        }

    }

    public void sendAllClients(String message) {

        int counter = 0;//zählt an welchem Index wir sind

        for (int i = 0; i < list_Clients.size(); i++) {

            if (checkListAtIndex(counter)) { //nur wenn es nach dem Aussortieren noch den Eintrag gibt

                ClientManager cm = list_Clients.get(i);
                if (cm.hasKey()) {
                    BigInteger klar = new BigInteger(message.getBytes());
                    BigInteger geheim = r.encrypt(klar, cm.getPubKey());
                    cm.getPw().println(geheim.toString());

                } else {
                    cm.getPw().println(message);
                }
                cm.getPw().flush();

                counter++;
            /*PrintWriter writer = (PrintWriter) it.next();
            writer.println(message);
            writer.flush();*/
            }
        }
    }

    public Server() {
        System.out.println("Schlüsselpaar wird berechnet. Einen Moment bitte...");
        r = new RSA(new BigInteger("4"), new BigInteger("8"));
        timer = new Timer();
        timer.schedule(new PingTimer(this), 30000, 60000);
    }
}
