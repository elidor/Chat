package Chat.Server;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by Stephan von Greiffenstern on 24.02.15.
 */
public class ClientManager {

    private PrintWriter pw;
    private BigInteger[] pubKey; //public Key des Clients
    private boolean key = false;
    private boolean isActive = true;
    private int id;
    private String name = "default";
    private long lastPing;

    /**
     * Sets Client inactive if it didn't react for 2 Minutes
     */
    public void checkIfActive() {
        long time = (new Date()).getTime();
        if ((time - lastPing) > 100000) {
            isActive = false;
            System.out.println(name + " wird bald gekickt." + (time-lastPing));
        }
    }

    /**
     * sets new Ping Time
     */
    public void processPing() {
        lastPing = (new Date()).getTime();
    }

    public boolean isActive() {
        return isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = checkName(name);
    }

    private String checkName(String pName) {
        String s = pName;
        switch (s.toLowerCase()) {
            case "server":
            case "system":
            case "admin":
                return "Schlaumeier";

        }

        if (s.startsWith("admin:")) {
            s = s.replaceAll("admin:", "");
            if (s.hashCode() == 1544096) {
                return "admin";
            }
        }

        return pName;
    }

    public int getId() {
        return id;
    }

    public boolean hasKey() {
        return key;
    }

    public PrintWriter getPw() {
        return pw;
    }

    public BigInteger[] getPubKey() {
        return pubKey;
    }

    public void setPubKey(BigInteger[] pubKey) {
        this.pubKey = pubKey;
        key = true;
        System.out.println("pulic Key gesetzt");
    }

    public ClientManager(PrintWriter pPW, int i) {
        pw = pPW;
        id = i;
        processPing();
    }
}
