package Chat.Client;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Created by Stephan von Greiffenstern on 24.02.15.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        final FXMLLoader loader = new FXMLLoader(getClass().getResource("Gui.fxml"));
        final Parent root = (Parent) loader.load();
        Scene scene = new Scene(root);
        primaryStage.setTitle("Supertolles Chatprogramm");
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {

            }
        });

    }

    public static void main(String[] args) {
        launch(args);
    }
}
