package Chat.Client;


import Chat.Verschlüsselung.RSA;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Stephan von Greiffenstern on 24.02.15.
 */
public class ControllerClient implements Initializable {
    @FXML
    private ListView listView;
    @FXML
    private TextArea recMes;
    @FXML
    private TextField sendMes;
    @FXML
    private TextField tfName;

    private Socket client;
    private PrintWriter writer;
    private BufferedReader reader;
    private Thread t1;
    private RSA r;
    private BigInteger[] pubKeyServer; //Public Key des Servers
    private boolean serverKey = false;

    public boolean connectToServer() {
        try {
            //client = new Socket("127.0.0.1", 5555);
            client = new Socket("127.0.0.1", 5555);
            reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            writer = new PrintWriter(client.getOutputStream());
            appendTextMessages("Netzwerkverbindung hergestellt");
            BigInteger[] bi = r.getPublicKey();
            writer.println("#pk:" + bi[0].toString() + "##" + bi[1].toString());
            writer.flush();

            return true;
        } catch (Exception e) {
            appendTextMessages("Netzwerkverbindung konnte nicht hergestellt werden");
            e.printStackTrace();

            return false;
        }
    }

    public void sendMessageToServer() {
        if (serverKey) {
            String msg = sendMes.getText();
            BigInteger klar = new BigInteger(msg.getBytes());
            BigInteger geheim = r.encrypt(klar, pubKeyServer);
            writer.println(geheim.toString());
            writer.flush();
        } else {
            writer.println(tfName.getText() + ": " + sendMes.getText());
            writer.flush();
            sendMes.setText("");
            sendMes.requestFocus();
            System.out.println("Nachrich unverschlüsselt gesendet!");
        }
        sendMes.setText("");
    }

    public void sendMessageToServer(String s) {
        if (serverKey) {
            BigInteger klar = new BigInteger(s.getBytes());
            BigInteger geheim = r.encrypt(klar, pubKeyServer);
            writer.println(geheim.toString());
            writer.flush();
        } else {
            writer.println(tfName.getText() + ": " + s);
            writer.flush();
            sendMes.setText("");
            sendMes.requestFocus();
            System.out.println("Nachrich unverschlüsselt gesendet!");
        }
    }

    public void appendTextMessages(String pMessage) {
        final String pMsg = pMessage;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                recMes.appendText(pMsg + "\n");
            }
        });
        //recMes.appendText(pMessage + "\n");
    }


    @FXML
    public void handleSendMessage(ActionEvent e) {
        sendMessageToServer();

    }

    @FXML
    public void handleButtonSend(ActionEvent actionEvent) {
        sendMessageToServer();
    }

    @FXML
    public void handleSumbitName(ActionEvent actionEvent) {
        sendMessageToServer("#nm:" + tfName.getText());
    }

    public void processMsg(String pMsg) {
        if (pMsg.startsWith("#ping:")) {
            sendMessageToServer("#ping:");
        } else if (pMsg.startsWith("#warning:")) {
            appendTextMessages("System: Verwarnung - " + pMsg.replaceAll("#warning:", ""));
        } else if (pMsg.startsWith("#list:")) {
            String msg = pMsg.replaceAll("#list:", "");
            String[] s = msg.split(",");
            List<String> al = Arrays.asList(s);
            final ObservableList<String> ol = FXCollections.observableArrayList(al);

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    listView.setItems(ol);
                }
            });
        } else {
            appendTextMessages(pMsg);
        }
    }


    public class MessagesFromServerListener implements Runnable {

        TextArea ta;

        @Override
        public void run() {
            String message;


            try {
                while ((message = reader.readLine()) != null) {
                    if (!serverKey && message.contains("#pk:")) {
                        String msg = message.replaceAll("#pk:", "");
                        String[] sp = msg.split("##");
                        BigInteger[] bi = new BigInteger[2];
                        bi[0] = new BigInteger(sp[0]);
                        bi[1] = new BigInteger(sp[1]);
                        pubKeyServer = bi;
                        serverKey = true;
                    } else if (serverKey) {
                        BigInteger bi = new BigInteger(message);
                        BigInteger decr = r.decrypt(bi);                        //entschlüsseln
                        String msg = new String(decr.toByteArray(), "UTF-8");   //entschlüsselten Text zu String konvertieren´
                        processMsg(msg);

                    } else {
                        System.out.println("unverschlüsselt empfangen");
                        appendTextMessagesThread(message);
                    }
                }
            } catch (IOException e) {
                appendTextMessagesThread("Nachricht konnte nicht empfangen werden!");
                e.printStackTrace();
            }
        }

        public void appendTextMessagesThread(final String pMsg) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    recMes.appendText(pMsg + "\n");
                }
            });
        }

        public MessagesFromServerListener(TextArea pTextArea) {
            ta = pTextArea;
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (connectToServer()) {
            appendTextMessages("Verbindung hergestellt.");
            t1 = new Thread(new MessagesFromServerListener(recMes));
            t1.setDaemon(true);
            t1.start();
        } else {
            appendTextMessages("Es gibt ein Problem mit der Verbindung.");
        }

    }

    public ControllerClient() {
        System.out.println("Schlüsselpaar wird berechnet...");
        r = new RSA(new BigInteger("4"), new BigInteger("8"));
    }
}
